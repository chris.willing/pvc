/* SPDX-License-Identifier: GPL-3.0-or-later */

/*
  This one is unusual in that there is no versioned tarball available.
  Therefore we parse the unpacked tarball (like tar -t) and extract
  the version number from the first entry which is the name & version
  of the directory containing the source code.
*/
const http = require('https');
const gunzip = require('zlib').createGunzip();
const tar = require('tar-stream');
var extract = tar.extract();
const common = require(pvcStartDir + '/pvcCommon.js');


var check = function (parent, options) {
  var action = options.action || 'check';
  var config = options.config || null;
  var entries = [];
  var versions = [];


  var reqpath = 'datafiles/release/' + parent.urlbase + '.tar.gz';
  var requestOptions = {
    headers: {
      'User-Agent': 'pvc: Project Version Checker'
    },
    host: 'invisible-island.net',
    path: '/' + reqpath
  };
  pvcDebug("Request: " + requestOptions.path);

  var req = http.get(requestOptions, function(response) {
    var versions = [];
    var version = '';

    response.pipe(gunzip).pipe(extract).on('entry', function (header, stream, next) {

      version = header.name.replace('diffstat-', "").replace('/', "");
      if ( version.length > 1 ) {
        versions.push(version);
        // Stop parsing as soon as we have a version
        stream.destroy();
      }
    
      // Not sure if this needed since we're breaking after first entry anyway
      stream.resume(); // just auto drain the stream

      stream.on('end', function() {

        switch (action) {
          case 'update':
            if (! versions) {
              eventEmitter.emit('UpdateWatcher', parent, void 0);
            } else {
              eventEmitter.emit('UpdateWatcher', parent, versions[0]);
            }
            break
          case 'validate':
            if (! versions) {
              pvcDebug("Request returned: " + res_data, 'PVC_ERROR');
              eventEmitter.emit('NotValidWatcher', parent);
            } else {
              if (versions[0] != parent.version) {
                pvcDebug("NOTE: latest version is " + versions[0]);
                parent.version = versions[0];
              }
              eventEmitter.emit('IsValidWatcher', parent);
            }
            break;
          case 'check':
          default:
            if (! versions) {
              eventEmitter.emit('CheckedWatcher', parent, void 0);
            } else {
              versions.sort( function(a,b) { return naturalCompare(b, a); });
              eventEmitter.emit('CheckedWatcher', parent, versions[0]);
            }
            break;
        }
      }.bind(parent));
    }.bind(parent));
  }.bind(parent));

  req.on('error', function(e) {
    console.log("(" + parent.project + ":" + parent.type + ":" + parent.urlbase + ") Error: " + e.message);
    eventEmitter.emit('CheckedWatcher', parent, void 0);
  }.bind(parent));
}

iisland_functions = {
  "check":check
}

/* ex:set ai shiftwidth=2 inputtab=spaces smarttab noautotab: */

