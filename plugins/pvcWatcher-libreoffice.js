/* SPDX-License-Identifier: GPL-3.0-or-later */

/*
  We need to first retrieve a short_version number e.g. 24.8.4
  Then use that to search download directory for full version
*/

const https = require('https');
const common = require(pvcStartDir + '/pvcCommon.js');

var check = function (parent, options) {
  var action = options.action || 'check';
  pvcDebug(`check() for project ${parent.project}`);

  // https request
  //var reqpath = 'download/' + parent.urlbase + '/';
  var reqpath = `download/${parent.urlbase}/`;

  var requestOptions = {
    headers: {
      'User-Agent': 'pvc: Project Version Checker'
    },
    host: 'www.libreoffice.org',
    port: 443,
    path: `/${reqpath}`
  };
  pvcDebug(`Request: ${requestOptions.path}`);
  var req = https.get(requestOptions, function(response) {
    // handle the response
    var res_data = '';
    response.on('data', function(chunk) {
      pvcDebug(`.....chunk`);
      res_data += chunk;
    });
    response.on('end', function() {
      res_data = res_data.split(/\r?\n/);
      pvcDebug(res_data);

      var tarballName;
      var version;
      for(var i in res_data) {
          pvcDebug(`:::: ${res_data[i]}`);
          var lomatch = res_data[i].match(/LibreOffice_\d+.\d+.\d+/);
          pvcDebug(`>>>> ${typeof(lomatch)} ${lomatch}`);
          if (lomatch) {
            pvcDebug(`>>>> ${typeof(lomatch)} ${lomatch} ${JSON.stringify(lomatch)}, ${lomatch[0]}`);
	    var short_version = lomatch[0].replace(/LibreOffice_/, "");
            break;
          }
      }

      if( short_version ) {
        eventEmitter.emit('HaveShortVersion', short_version);
      }

    }.bind(parent)); // reponse.on //
  }.bind(parent));   // https.get //

  req.on('error', function(e) {
    console.log(`(${parent.project}: ${parent.type}: ${parent.urlbase}) Error: ${e.message}`);
    eventEmitter.emit('CheckedWatcher', parent, void 0);
  }.bind(parent));

  /*
  *  Apparently we found the short_version
  *  which we can use (with a new https query)
  *  to search the relevant download directory for a versioned
  *  source tarball from which we extract full version string
  */
  eventEmitter.on('HaveShortVersion', (short_version) => {
    pvcDebug(`Have short version: ${short_version}`);

    requestOptions.host = `download.documentfoundation.org`;
    requestOptions.path = `/libreoffice/src/${short_version}/`;
    pvcDebug(`requestOptions changed to: ${JSON.stringify(requestOptions)}`);
    var req = https.get(requestOptions, function(response) {
      // handle the response
      var res_data = '';
      response.on('data', function(chunk) {
        pvcDebug(`.....chunk`);
        res_data += chunk;
      });
      response.on('end', function() {
        res_data = res_data.split(/\r?\n/);
        pvcDebug(res_data);

        var tarballName;
        var version;
        for(var i in res_data) {
            pvcDebug(`:::: ${res_data[i]}`);
            var lomatch = res_data[i].replace(/\?/,"   ").match(/>libreoffice-\d+.*\d+\.tar\.[a-z]*</);
            pvcDebug(`>>>> ${typeof(lomatch)} ${lomatch}`);
            if (lomatch) {
              pvcDebug(`>>>> length = ${lomatch.length} for ${lomatch}`);
              tarballName = lomatch[lomatch.length-1].replace(/>|</g,"");
              pvcDebug(`Found tarballName: ${tarballName}`);
              break;
            }
        }
        if (tarballName ) {
          version = tarballName.replace(/^[^0-9]*|[^0-9]*$/g, "");
	  pvcDebug(`Detected tarball version = ${version}`);
        }

        switch (action) {
          case 'update':
            if (! version) {
              eventEmitter.emit('UpdateWatcher', parent, void 0);
            } else {
              eventEmitter.emit('UpdateWatcher', parent, version);
            }
            break;
          case 'validate':
            if (! version) {
              pvcDebug(`tarball download not found in ${res_data}: PVC_ERROR`);
              eventEmitter.emit('NotValidWatcher', parent);
            } else {
              if ( version != parent.version ) {
                pvcDebug(`NOTE: latest version is ${version}`);
                parent.version = version;
              }
              eventEmitter.emit('IsValidWatcher', parent);
            }
            break;
          case 'check':
          default:
            if (! version) {
              eventEmitter.emit('CheckedWatcher', parent, void 0);
            } else {
              eventEmitter.emit('CheckedWatcher', parent, version)
            }
            break;
        } // switch //

      }.bind(parent));	// response.on 'end'
    }.bind(parent));	// hhtps.get
      
  });	// eventEmitter.on 'HaveShortVersion'

}


libreoffice_functions = {
  "check":check,
};


/* ex:set ai shiftwidth=2 inputtab=spaces smarttab noautotab: */

